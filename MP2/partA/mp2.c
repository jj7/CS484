#include "mp2-helper.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Sequential solver using red-black method
void sequential_solver(int asize) {
    
    int p_asize = asize+2;
    float *A;
    init_array(asize,&A);
    int it, i, j;
    
    for(it = 0; it < MAXIT; ++it) {
        reset_peak_seq(asize, &A);
        // Red
        bool alt = false;
        for (i = 1; i < p_asize-1; i++) {
            for (j = !alt ? 1 : 2; j < p_asize-1; j+=2) {
                A[IND(i,j,p_asize)] = 0.20*(
                                            A[IND(i,j,p_asize)] +
                                            A[IND(i-1,j,p_asize)] +
                                            A[IND(i+1,j,p_asize)] +
                                            A[IND(i,j-1,p_asize)] +
                                            A[IND(i,j+1,p_asize)]);
            }
            alt = !alt;
        }
        
        // Black
        alt = true;
        for (i = 1; i < p_asize-1; i++) {
            for (j = !alt ? 1 : 2; j < p_asize-1; j+=2) {
                A[IND(i,j,p_asize)] = 0.20*(
                                            A[IND(i,j,p_asize)] +
                                            A[IND(i-1,j,p_asize)] +
                                            A[IND(i+1,j,p_asize)] +
                                            A[IND(i,j-1,p_asize)] +
                                            A[IND(i,j+1,p_asize)]);
            }
            alt = !alt;
        }
        
        double iteration_sum = 0.0f;
        
#ifdef REPORT_SUM
        for(i = 1; i < p_asize-1; i++)
            for(j = 1; j < p_asize-1; j++)
                iteration_sum += A[IND(i,j,p_asize)];
#endif
        
        report_iteration(-1, it, iteration_sum, &A);
    }
    
    clean_arrays(&A);
}

// Parallel solver using MPI and red-black method
void parallel_solver(int asize, int tileSizeX, int tileSizeY) {
    int num_total_blocks=asize*asize/(tileSizeX*tileSizeY);
    int myRank;
    
    // Subarray initialization with padding
    // Use A for the output array
    float *A;
    float temp_h[tileSizeX];//horizontal
    float temp_v[tileSizeY];//vertical
    init_subarrays(myRank, tileSizeX, tileSizeY, &A);
    
    //compute the tile number
    /*int i_starts[num_total_blocks], i_ends[num_total_blocks];
    int j_starts[num_total_blocks], j_ends[num_total_blocks];*/
    
    /*int t;
    for(t=0;t<num_total_blocks;t++)
    {
        int column = t % column_count;//not phisical
        int row = t / column_count;
        
        i_starts[t] = 1 + column* tileSizeX;
        i_ends[t] = i_starts[t] + tileSizeX;
        j_starts[t] = 1 + row * tileSizeY;
        j_ends[t] = j_starts[t] + tileSizeY;
    }*/
    
    MPI_Status status, waitstatus;
    MPI_Request right,left,down,up;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);//get rank id
    
    int X=tileSizeX+2;
    int Y=tileSizeY+2;
    int column_count=(asize / tileSizeX);
    int row_count=(asize/tileSizeY);
    
    
    MPI_Datatype COLUMN, foo;
    MPI_Type_vector(Y, 1, X, MPI_FLOAT, &foo);
    MPI_Type_create_resized(foo, 0, sizeof(float), &COLUMN);
    MPI_Type_commit(&COLUMN);

    int it;
    int i,j;
    for(it = 0; it < MAXIT; it++)
    {
        // Reset peak at the beginning of each iteration
        reset_peak(myRank, tileSizeX, &A);
        /*
         * YOUR PARALLEL CODE GOES HERE
         */
        //red
        //the new size for the subarray is (tileSizeY+2)*(tileSizeX+2)
        
        //red
        bool alt = false;
        for (i = 1; i < (Y-1); i++) {
            for (j = !alt ? 1 : 2; j < (X-1); j+=2) {
                A[IND(i,j,X)] = 0.20*(A[IND(i,j,X)] +
                                              A[IND(i-1,j,X)] +
                                              A[IND(i+1,j,X)] +
                                              A[IND(i,j-1,X)] +
                                              A[IND(i,j+1,X)]);
            }
            alt = !alt;
        }
        
        
        //SEND BLOCK
        //send vertical boundary to the right as long as not the right most column
        if(myRank%column_count<(column_count-1)){

            MPI_Isend(&A[IND(0,tileSizeX, X)],1,COLUMN,myRank+1,it,MPI_COMM_WORLD,&right);//send to the right
           
        }
        
        
        //send vertical boundary to the left as long as not the left most column
       if(myRank%column_count>0){

            MPI_Isend(&A[IND(0,1, X)],1,COLUMN,myRank-1,it,MPI_COMM_WORLD,&left);
        }
        
        //send horizontal boundary to the downside as long as not the last row
        if(myRank/column_count<(row_count-1)){
           
            MPI_Isend(&A[IND(tileSizeY,0, X)],X,MPI_FLOAT,myRank+column_count,it,MPI_COMM_WORLD,&down);
        }
        
        //send horizontal boundary to the upside as long as not the first row
        if(myRank/column_count>0){
            
            MPI_Isend(&A[IND(1,0, X)],X,MPI_FLOAT,myRank-column_count,it,MPI_COMM_WORLD,&up);
        }
        
        
        //RECEIVE BLOCK
        //also receive boundary from the right
        if(myRank%column_count<(column_count-1)){
            MPI_Recv(&A[IND(0,X-1,X)],1,COLUMN,myRank+1,it,MPI_COMM_WORLD,&status);//receive from the right
                   }
       //also receive boundary from the left
        if(myRank%column_count>0){
            MPI_Recv(&A[IND(0,0,X)],1,COLUMN,myRank-1,it,MPI_COMM_WORLD,&status);
           
        }
         //also receive boundary from the downside
        if(myRank/column_count<(row_count-1)){
            MPI_Recv(&A[IND(X-1,0,X)],X,MPI_FLOAT,myRank+column_count,it,MPI_COMM_WORLD,&status);
           
        }
        //also receive boundary from the upside
        if(myRank/column_count>0){
            MPI_Recv(&A[IND(0,0,X)],X,MPI_FLOAT,myRank-column_count,it,MPI_COMM_WORLD,&status);
        }
        
        if(myRank%column_count<(column_count-1)){
            MPI_Wait(&right,&waitstatus);
        }
         if(myRank%column_count>0){
             MPI_Wait(&left,&waitstatus);
         }
        if(myRank/column_count<(row_count-1)){
             MPI_Wait(&down,&waitstatus);
        }
        if(myRank/column_count>0){
            MPI_Wait(&up,&waitstatus);
        }
    
               //black
        alt = true;
        for (i = 1; i < (Y-1); i++) {
            for (j = !alt ? 1 : 2; j < (X-1); j+=2) {
                A[IND(i,j,X)] = 0.20*(A[IND(i,j,X)] +
                                      A[IND(i-1,j,X)] +
                                      A[IND(i+1,j,X)] +
                                      A[IND(i,j-1,X)] +
                                      A[IND(i,j+1,X)]);
            }
            alt = !alt;
        }

        //MPI_Barrier(MPI_COMM_WORLD);
        
        //another bunch of boundary exchanges
        //SEND BLOCK
        //send vertical boundary to the right as long as not the right most column
        if(myRank%column_count<(column_count-1)){
            
            MPI_Isend(&A[IND(0,tileSizeX, X)],1,COLUMN,myRank+1,it+1,MPI_COMM_WORLD,&right);//send to the right
            
        }
        
        
        //send vertical boundary to the left as long as not the left most column
        if(myRank%column_count>0){
            
            MPI_Isend(&A[IND(0,1, X)],1,COLUMN,myRank-1,it+1,MPI_COMM_WORLD,&left);
        }
        
        //send horizontal boundary to the downside as long as not the last row
        if(myRank/column_count<(row_count-1)){
            
            MPI_Isend(&A[IND(tileSizeY,0, X)],X,MPI_FLOAT,myRank+column_count,it+1,MPI_COMM_WORLD,&down);
        }
        
        //send horizontal boundary to the upside as long as not the first row
        if(myRank/column_count>0){
            
            MPI_Isend(&A[IND(1,0, X)],X,MPI_FLOAT,myRank-column_count,it+1,MPI_COMM_WORLD,&up);
        }
        
        
        //RECEIVE BLOCK
        //also receive boundary from the right
        if(myRank%column_count<(column_count-1)){
            MPI_Recv(&A[IND(0,X-1,X)],1,COLUMN,myRank+1,it+1,MPI_COMM_WORLD,&status);//receive from the right
        }
        //also receive boundary from the left
        if(myRank%column_count>0){
            MPI_Recv(&A[IND(0,0,X)],1,COLUMN,myRank-1,it+1,MPI_COMM_WORLD,&status);
            
        }
        //also receive boundary from the downside
        if(myRank/column_count<(row_count-1)){
            MPI_Recv(&A[IND(X-1,0,X)],X,MPI_FLOAT,myRank+column_count,it+1,MPI_COMM_WORLD,&status);
            
        }
        //also receive boundary from the upside
        if(myRank/column_count>0){
            MPI_Recv(&A[IND(0,0,X)],X,MPI_FLOAT,myRank-column_count,it+1,MPI_COMM_WORLD,&status);
        }
        
        if(myRank%column_count<(column_count-1)){
            MPI_Wait(&right,&waitstatus);
        }
        if(myRank%column_count>0){
            MPI_Wait(&left,&waitstatus);
        }
        if(myRank/column_count<(row_count-1)){
            MPI_Wait(&down,&waitstatus);
        }
        if(myRank/column_count>0){
            MPI_Wait(&up,&waitstatus);
        }

     
        /* INSTRUCTION: Add code below that will aggregate sum of all processes'
         *              subarrays into the variable iter_sum
         */
        double iter_sum = 0.0;
        
#ifdef REPORT_SUM
        /*
         *  Sum all cells in all processes and
         *  store it to Rank 0's iter_sum
         */
        for(i = 1; i < tileSizeY+1; i++)
            for(j = 1; j < tileSizeX+1; j++)
                iter_sum += A[IND(i,j,X)];
#endif
        double globalcount=0.0;
        
        MPI_Reduce(&iter_sum,&globalcount,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        
        // Report on iteration: DO NOT REMOVE THIS LINE
        report_iteration(myRank, it, globalcount, &A);
    }
    
    clean_arrays(&A);
}

int main(int argc, char** argv) {
    
    int asize = 8;
    int tile_size_x = 4;
    int tile_size_y = 4;
    
    int nProcs;
    int myRank;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    
    if (argc < 4) {
        if(!myRank) {
            printf("USAGE: mpirun -np <Number of Processes> ./mp2 <Matrix Dimension> <Tile Size X> <Tile Size Y>\n");
            MPI_Finalize();
            return -1;
        }
    }
    
    if (argc == 4) {
        asize = atoi(argv[1]);
        tile_size_x = atoi(argv[2]);
        tile_size_y = atoi(argv[3]);
    }
    
    // Run and time the sequential version
    
#ifdef RUN_SEQ
    if(!myRank) {
        time_seqn(asize);
    }
    
    // Let sequential finish
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    
    // Run and time the parallel version
    time_parallel(myRank, asize, tile_size_x, tile_size_y);
    
    MPI_Finalize();
    return 0;
}

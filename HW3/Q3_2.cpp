#include "mpi.h" 
#include <stdio.h> 
#include <iostream>
#include <stdlib.h>
#include <algorithm>sort


*double parallel_search(int k, double *Guess, bool flag, double &Array){
    int myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    double *new_guess=new double [2];
    int *largercount=new int [k];
    int *LessGlobal=new int [k];
    for(int j=0;j<k;j++){
        largercount[j]=0;
        LessGlobal[j]=0;
    }
    double threshold=1;
      
    int SubSize=ArraySize/nProcs;
    double *SubArray=new double[SubSize];
    
    initilize_SubArray(myRank,&SubArray,&Array);//assume there's a function to initialize
    sort(SubArray,SubArray+SubSize);
    
    //counting
    for(int j=0;j<k;j++){
        int lesscount=0;
        int largercount=0;
        for(int i=0;i<SubSize;i++){
            if SubArray[i]<=Guess[j]
                lesscount++;
            if SubArray[i]>Guess[j]
                largercount++;
        }
        MPI_Reduce(&lesscount,&(LessGlobal+j),1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
        MPI_Reduce(&lagercount,&(LargerGlobal+j),1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
    }

   if(!myRank){
        int *difference=new int [k];
        for(int j=0;j<k;j++){
            difference[j]=abs(LessGlobal-LargerGlobal);
           if(abs(LessGlobal[j]-LargerGlobal[j])<threshold){
            flag=false;
            return;
            }
        }
        int index1,index2;
        double temp1=difference[0];
        double temp2=difference[0];
        for(int j=0;j<k;j++){
           if(difference[j]<temp1){
                  temp1=difference[j];
                  index1=j;
              }
        }
        for(int j=0;j<k;j++){
           if(temp1<difference[j]<temp2){
                  temp2=difference[j];
                  index2=j;
              }
        }
        for(int j=0;j<k;j++){
            new_guess[j]=Guess[j]+(Guess[k-1]-Guess[0])/k*j;
        }

        MPI_Bcast(&new_guess,k,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }
   
   return *new_guess;
  }  


int main(){
    int nProcs;
    int myRank;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    
    int ArraySize=10000;
  
    double *Array=new double[ArraySize];
    
    int k;//Guess size each iteration
    
    double *Guess=new double [k];
    for(int j=0;j<k;j++){
        Guess[j]=rand();
    }


    bool flag=true;
    
    do{
    *Guess=parallel_search(k, Guess, flag,&Array);       
    }while(flag);
    
    if(!myRank){
    cout<<"The median is "<<Guess[k/2]<<endl;
    }
    


    MPI_Finalize();
    return 0;
}
#include "mpi.h" 
#include <stdio.h> 
#include <iostream>
#include <stdlib.h>
#include <algorithm>sort

void parallel_search(double Guess, bool flag, double &Array){
	int myRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	int lesscount=0;
	int largercount=0;
    
    double threshold=1;
    double Granular=0.1;
    
    int SubSize=ArraySize/nProcs;
    double *SubArray=new double[SubSize];
    
    initilize_SubArray(myRank,&SubArray,&Array);//assume there's a function to initialize
    
    sort(SubArray,SubArray+SubSize);
    //counting
	for(int i=0;i<SubSize;i++){
		if SubArray[i]<=Guess
			lesscount++;
		if SubArray[i]>Guess
			largercount++;
	}
    
    int LessGlobal=0;
    int LargerGlobal=0;
    MPI_Reduce(&lesscount,&LessGlobal,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
    MPI_Reduce(&lagercount,&LargerGlobal,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);

   if(!myRank){
        if(LessGlobal-LargerGlobal>=threshold)
    		Guess=Guess-Granular;
 		if(LargerGlobal-LessGlobal>=threshold)
    	    Guess=Guess+Granular;    
    	if(abs(LessGlobal-LargerGlobal)<threshold)
    		flag=false;
    }

  }  


int main(){
	int nProcs;
	int myRank;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    
    int ArraySize=10000;
  
    double *Array=new double[ArraySize];
    
    double Guess=rand();
    bool flag=true;
    
    do{
    parallel_search(Guess, flag, &Array);   	
    }while(flag);

   if(!myRank){
   	cout<<"The median is "<<Guess<<endl;
   }

 	MPI_Finalize();
	return 0;
}